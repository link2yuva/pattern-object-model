package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription ="Create Lead in leaftaps";
		testNodes = "Leads";
		authors ="Yuvaraj";
		category = "smoke";
		dataSheetName="TC001_CreateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void CreateLead(String uName, String pwd, String cName, String fName, String lName)
	{
		new LoginPage()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLead();			
	}

}
