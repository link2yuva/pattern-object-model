package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_EditLead";
		testDescription ="Edit Lead in leaftaps";
		testNodes = "Leads";
		authors ="Yuvaraj";
		category = "smoke";
		dataSheetName="TC002_EditLead";
	}
	
	@Test(dataProvider="fetchData")
	public void CreateLead(String uName, String pwd, String fName, String cName, String title)
	{
		new LoginPage()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeads()
		.enterFirstName(fName)
		.clickFindLeads()
		.clickLeadId()
		.VerifyTitle(title)
		.clickEditLead()
		.enterCompanyName(cName)
		.clickUpdate();
	}

}
