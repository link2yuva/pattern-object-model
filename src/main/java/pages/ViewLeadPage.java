package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.WebDriverEventListener;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH, using="//a[@class='subMenuButton' and text()='Edit']") WebElement eleEditLead;
	@FindBy(how=How.CLASS_NAME, using="subMenuButtonDangerous") WebElement eleDeleteLead;
	@FindBy(how=How.XPATH, using="//a[@class='subMenuButton' and text()='Duplicate Lead']") WebElement eleDuplicateLead;
	

	public EditLeadPage clickEditLead()
	{
		click(eleEditLead);
		return new EditLeadPage();
	}

	public MyLeadsPage clickDeleteLead()
	{
		click(eleDeleteLead);
		return new MyLeadsPage();
	}

	public DuplicateLeadPage clickDuplicateLead()
	{
		click(eleDuplicateLead);
		return new DuplicateLeadPage();
	}
	
	public ViewLeadPage VerifyTitle(String data)
	{
		verifyTitle(data);
		return this;
	}
}








