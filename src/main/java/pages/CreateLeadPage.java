package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.WebDriverEventListener;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how=How.NAME, using="submitButton") WebElement eleCreateLead;
	
	public CreateLeadPage enterCompanyName(String data)
	{
		type(eleCName, data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data)
	{
		type(eleFName, data);
		return this;
	}
	
	public CreateLeadPage enterLastName(String data)
	{
		type(eleLName, data);
		return this;
	}
	
	public ViewLeadPage clickCreateLead()
	{
		click(eleCreateLead);
		return new ViewLeadPage();
	}
	
}







