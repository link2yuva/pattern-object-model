package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.WebDriverEventListener;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.NAME, using="submitButton") WebElement eleUpdate;
	public EditLeadPage enterCompanyName(String data)
	{
		type(eleCompanyName, data);
		return this;
	}
		
	public ViewLeadPage clickUpdate()
	{
		click(eleUpdate);
		return new ViewLeadPage();
	}
}







